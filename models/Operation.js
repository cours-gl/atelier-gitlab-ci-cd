const mongoose = require('mongoose');

const operationSchema = new mongoose.Schema({
  a: {type: Number},
  b: {type: Number},
  operator: {type: String, trim: true},
  result: {type: Number},
  created_at: {type: Date, default: Date.now},
  updated_at: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Operation', operationSchema);