const {Compute} = require('../compute');

const operation = new Compute();

// test Compute.add function
test.each([[1, 1, 2], [-1, 2, 1], [-2, -1, -3]])(
    '%i + %i equals %i', (a, b, expected) => {
        expect(operation.add(a, b)).toBe(expected);
    }
);

// test Compute.sub function
test.each([[1, 1, 0], [-1, 1, -2], [5, 3, 2]])(
    '%i - %i equals %i', (a, b, expected) => {
        expect(operation.sub(a, b)).toBe(expected);
    }
);